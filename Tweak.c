#include <substrate.h>
#include <syslog.h>

static void f_TUAssertTrigger(const char* emsg) {
  syslog(LOG_WARNING,"TUAssert %s",emsg);
}

static __attribute__((constructor)) void init() {
  MSImageRef image=MSGetImageByName("/usr/lib/libTelephonyUtilDynamic.dylib");
  MSHookFunction(MSFindSymbol(image,"___TUAssertTrigger"),f_TUAssertTrigger,NULL);
}
